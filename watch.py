import os
import sys
import re
import json
import smtplib
from email.mime.text import MIMEText
import subprocess
import MySQLdb

node = '/home/ubuntu/.nvm/versions/node/v4.2.1/bin/node'

def send_mail(name, email, subject, message):
    if email == '' or subject == '' or message == '':
        return
    msg = MIMEText(message)

    msg['Subject'] = subject
    msg['From'] = "WikiEducator Bot <noreply@WikiEducator.org>"
    msg['To'] = "%s <%s>" % (name, email)

    s = smtplib.SMTP('localhost')
    s.sendmail('noreply@WikiEducator.org', [r['user_email']],
            msg.as_string())
    s.quit()

options = json.load(open('options.json', 'rt'))
db = MySQLdb.connect(host=options['mwdbhost'],
        user=options['mwuser'],
        passwd=options['mwpasswd'],
        db=options['mwdb'])
c = db.cursor(MySQLdb.cursors.DictCursor)

# find the most recent request that hasn't been started
c.execute("""SELECT * from mw_snapshot
             WHERE status=0
             ORDER BY id
             LIMIT 1""")
row = c.fetchone()
if row is None:
    sys.exit(0)

opt = json.loads(row['opt'])
inoutline = opt['outline'].strip()
if not inoutline.lower().startswith('http://wikieducator.org'):
    if inoutline[0] <> '/':
        inoutline = '/' + inoutline
    opt['outline'] = 'http://wikieducator.org' + inoutline

# mark in progress
c.execute("""UPDATE mw_snapshot
             SET status=1, started=NOW()
             WHERE id=%s""",
             (row['id'],))
db.commit()

result = 0
errmsg = ''
# build a title out of the outline page name
title_parts = opt['outline'].split('/')
if len(title_parts) >= 2:
    title = title_parts[-2]
else:
    title = title_parts[-1]
title = title.replace('_', ' ')
title = title.replace('Practice:', '')
title = '/' + title

os.chdir('/home/ubuntu/Projects/course')

if not 'type' in opt:
    result = 101
    errmsg = 'Request type missing'
elif opt['type'] == 'wp':
    args = [
            node, 'courseWP.js',
            '--prevnext',
            '--brand', 'DS4OER Challenge',
            '--wpurl', opt['url'],
            '--wpuser', opt['user'],
            '--wppass', opt['password'],
            '--wpmenu', '--wpdelete',
            '--theme', 'newSplashWP',
            ]
elif opt['type'] == 'fs':
    title = '/' + opt['course']
    args = [
            '/usr/local/bin/node', 'course.js',
            '--prevnext',
            '--urlprefix', title,
            '--brand', title[1:],
            '--theme', 'newSplash',
            ]
else:
    result = 102
    errmsg = 'Unknown snapshot type requested'

if 'logo' in opt and opt['logo']:
    args.extend(['--logo', opt['logo'],
            '--link', opt['link']])
args.extend([opt['outline'], title])

#print '======== args ========='
#print args
log = open(options.get('log', '/dev/null'), 'a')
err = open(options.get('errlog', '/dev/null'), 'a')

result = subprocess.call(args, stdout=log, stderr=err)
#print "Result:", result

c.execute("""UPDATE mw_snapshot
             SET status=2, result=%s, completed=NOW()
             WHERE id=%s""",
             (result, row['id'],))
db.commit()

c.execute("""SELECT user_real_name, user_email,
                    user_email_authenticated
             FROM mw_user where user_id=%s""",
             (row['userid'],))
r = c.fetchone()
if r is None or r['user_email_authenticated'] == '':
    sys.exit(0)
name = r['user_real_name']
email = r['user_email']
if result == 0:
    send_mail(name, email,
              'WikiEducator->WordPress course snapshot created',
               """A snapshot of %s\n
    has been created at %s\n""" % (opt['outline'].encode('utf-8'), opt['url'].encode('utf-8'))
    )
elif result == 11:
    send_mail(name, email,
              'WikiEducator->WordPress course snapshot created',
               """A snapshot of %s\n
has been created at %s\nbut there may have been a problem creating the navigation menu.\n""" % (opt['outline'].encode('utf-8'), opt['url'].encode('utf-8'))
    )
else:
    send_mail(name, email,
              'WikiEducator->WordPress course snapshot FAILED',
               """Unable to create a snapshot of %s\n
Please check the URL, username, and password.\n"""
    % (opt['outline'].encode('utf-8'), )
    )


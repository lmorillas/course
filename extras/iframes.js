console.log('in iframe.js!');

module.exports = {
    version: "0.1.0",
    process: function($, opt) {
        console.log('in iframe.js module!');
        var ifcount = 0,
            iframe,
            ifurl,
            ifheight,
            ifwidth,
            ifstyle,
            ifstyle_string,
            ifclasses,
            ifid;
        // find the nodes in the Dom...
        $(".WEiFrame").each(function() {
            iframe = ifurl = ifheight = ifwidth = ifstyle = ifstyle_string = ifclasses = ifid = "";
            ifcount += 1;
            console.log('WEiFrame '+ifcount+' ...');
            $(this).find("div").each(function() {
                ifclass = $(this).attr('class');
                //console.log('found '+ifclass+'='+$(this).text());
                switch($(this).attr('class')) {
                    case 'url':
                        ifurl = $(this).text();
                        console.log('url = '+ifurl);
                        iframe += ' src="'+ifurl+'"';
                        break;
                    case 'height':
                        ifheight = $(this).text();
                        console.log('height = '+ifheight);
                        ifstyle_string += ' height:'+ifheight+';';
                        break;
                    case 'width':
                        ifwidth = $(this).text();
                        console.log('width = '+ifwidth);
                        ifstyle_string += ' width:'+ifwidth+';';
                        break;
                    case 'style':
                        ifstyle = $(this).text();
                        console.log('style = '+ifstyle);
                        ifstyle_string += ' '+ifstyle;
                        break;
                    case 'classes':
                        ifclasses = $(this).text();
                        console.log('classes = '+ifclasses);
                        iframe += ' class="'+ifclasses+'"';
                        break;
                    case 'id':
                        ifid = $(this).text();
                        console.log('id = '+ifid);
                        iframe += ' id="'+ifid+'"';
                        break;
                }
            });
            if (ifstyle_string != "") {
                iframe += ' style="'+ifstyle_string+'"';
            }
            //iframe = '<div class="WEiFrame"><iframe frameborder="0" '+iframe+' /></div>';
            iframe = '<div class="WEiFrame"><iframe '+iframe+' /></div>';
            iframe = '<!-- Escape editor -->'+iframe+'<!-- Un-Escape editor -->';
            console.log(ifcount+'. '+iframe);
            $(this).replaceWith(iframe);
        });
    }
}

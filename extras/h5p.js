console.log('in h5p.js!');

module.exports = {
    version: "0.0.2",
    process: function($, opt) {
        console.log('in h5p.js module!');
        var count = 0,
            h5pid,
            host,
            subdir,
            subsite,
            height,
            width,
            style,
            style_string,
            classes,
            id;
        // find the nodes in the Dom...
        $(".WEH5P").each(function() {
            h5pid = host = subdir = subsite = height = width = style = style_string = classes = id = "";
            code = "";
            count += 1;
            console.log('WEH5P '+count+' ...');
            $(this).find("div").each(function() {
                console.log('found class = '+$(this).attr('class'));
                switch($(this).attr('class')) {
                    case 'h5pid':
                        h5pid = $(this).text();
                        console.log('h5pid = '+h5pid);
                        break;
                    case 'host':
                        host = $(this).text();
                        /*origin = window.location.origin;
                        if (host === origin) {
                            host = '';
                            console.log('same host!');
                        } else {
                            console.log('host = '+host);
                        }*/
                        break;
                    case 'subsite':
                        subsite = $(this).text();
                        console.log('subsite = '+subsite);
                        break;
                    case 'subdir':
                        subdir = $(this).text();
                        console.log('subdir = '+subdir);
                        break;
                    case 'height':
                        height = $(this).text();
                        console.log('height = '+height);
                        style_string += ' height:'+height+';';
                        break;
                    case 'width':
                        width = $(this).text();
                        console.log('width = '+width);
                        style_string += ' width:'+width+';';
                        break;
                    case 'style':
                        style = $(this).text();
                        console.log('style = '+style);
                        style_string += ' '+style+';';
                        break;
                    case 'classes':
                        classes = $(this).text();
                        console.log('classes = '+classes);
                        break;
                    case 'id':
                        id = $(this).text();
                        console.log('id = '+id);
                        break;
                }
            });
            script_url = host;
            if (subsite != "") {
                script_url = subsite+'.'+script_url;
            }
            if (subdir != "") {
                script_url = script_url+'/'+subdir;
            }
            if (classes != "") {
                // add a space...
                classes += ' ';
            }
            classes += 'h5p-iframe h5p-initialized WEH5P';
            if (id != "") {
                // add a space...
                id += ' ';
            }
            id += "h5p-iframe-'+count+'";
            code = '<iframe id="'+id+'" class="'+classes+'"';
            code += ' data-content-id="'+h5pid+'" src="'+script_url+'/wp-admin/admin-ajax.php?action=h5p_embed&amp;id='+h5pid+'"';
            if (width != "") {
                code += ' width="'+width+'"';
            }
            if (height != "") {
                code += ' height="'+height+'"';
            }
            if (style_string != "") {
                code += ' style="'+style_string+'"';
            }
            //code += ' width="1108" height="244"';
            code += ' frameboarder="0" allowfullscreen="allowfullscreen"></iframe>';
            code += '<script class="WEH5P" src="'+script_url+'/wp-content/plugins/h5p/h5p-php-library/js/h5p-resizer.js" charset="UTF-8">&nbsp;</script>';
            //code = '<!-- Escape editor -->'+code+'<!-- Un-Escape editor -->';
            console.log('code = '+code);
            $(this).replaceWith(code);
        });
    }
}

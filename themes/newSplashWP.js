/* jshint node: true, multistr: true, trailing: true, esnext: true */
function quotedWPstring(s) {
  if (s.indexOf('"') === -1) {
    return `"${s}"`;
  } else if (s.indexOf("'") === -1) {
    return `'${s}'`;
  }
  return `'${s.replace(/'/g, "\\'")}'`;
}

module.exports = {
  version: "0.2.0",

  // emit headers
  header: function($, opt) {
    var p = opt["--urlprefix"] + "/";
    $('head link[href*="wikieducator.org/load.php"]').remove();
    $('head link[rel="shortcut icon"]').attr("href", p + "img/favicon.png");
    $("head").append(`
  <!-- Bootstrap -->
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,200,700" rel="stylesheet" type="text/css">
  <link rel="stylesheet" type="text/css" href="' + p +'css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="' + p +'css/layout.css">
  <link rel="stylesheet" type="text/css" href="' + p +'css/typography.css">
  <link rel="stylesheet" type="text/css" href="' + p +'css/colours.css">
  <link rel="stylesheet" type="text/css" href="' + p +'css/we.css">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js does not work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <!-- jQuery (necessary for Bootstrap JavaScript plugins) -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="' + p +'js/bootstrap.min.js"></script>

  <script src="' + p + 'js/we.js"></script>
`);
  },

  footer: function($, opt, pages, pi, outlineItem) {
    // use silver CC-BY-* buttons instead of Free Cultural Works orange
    var iconsrc = $("#footer-copyrightico img")
      .attr("src")
      .replace("/icons/", "/buttons/");
    var footerlogo = "",
      contentURL = `https://wikieducator.org${outlineItem.wkpage}`;
    if (opt["--logo"]) {
      footerlogo = `<img src="${opt[
        "--logo"
      ]}" alt="institution logo" style="float: right;">`;
      if (opt["--link"]) {
        footerlogo = `<a href="${opt["--link"]}">${footerlogo}</a>`;
      }
    }
    $(".printfooter").remove();
    $("body").append(`
<footer>
[oeru_advanced_footer content='${footerlogo}
<a href="${contentURL}" title="original content in WikiEducator" target="WikiEducator">Content</a> is available under the
<a rel="license" href="https://wikieducator.org/WikiEducator:Copyrights" title="WikiEducator:Copyrights">Creative Commons Attribution Share Alike License</a>.
<a href="https://wikieducator.org/WikiEducator:Privacy_policy">Privacy Policy</a> | <a href="https://wikieducator.org${outlineItem.wkpage}?action=history">Authors</a><a href="http://creativecommons.org/licenses/by-sa/3.0/"><img class="cc" src="${iconsrc}" alt="Creative Commons Attribution Share-Alike License"></a>']
</footer>`);
    $("footer img.cc").attr("src", iconsrc);
  },

  wrap: function($) {
    $("#content").addClass("container").wrap('<section id="main"></section>');
    $("#content")
      .wrapInner('<div class="panel-body"/>')
      .wrapInner('<div class="panel"/>')
      .wrapInner('<div class="col-md-12"/>')
      .wrapInner('<div class="row"/>');
  },

  nav: function($, opt, pages, pi, outline, getPrev, getNext) {
    var i,
      l,
      j,
      l2,
      l2start,
      l2end,
      pno,
      actives,
      pinfo,
      psinfo,
      subclasses,
      classes = [];
    var nb =
      '<header>\n\
  <div>\n\
    <div class="container">\n\
      <div class="brandtext">\n\
        <h1><a href="' +
      opt["--urlprefix"] +
      '">' +
      opt["--brand"] +
      '</a></h1>\n\
      </div>\n\
    </div>\n\
  </div>\n\
  <div class="container">\n\
    <nav class="navbar" role="navigation">\n\
      <div class="navbar-header">\n\
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">\n\
          <span class="sr-only">Toggle navigation</span>\n\
          <span class="icon-bar"></span>\n\
          <span class="icon-bar"></span>\n\
          <span class="icon-bar"></span>\n\
        </button>\n\
      </div>\n\
      <div class="collapse navbar-collapse navbar-ex1-collapse">\n\
        <ul class="nav navbar-nav">\n';

    l = outline.length;
    actives = outline[pi].path;
    for (i = 1; i < l; i++) {
      classes = [];
      if (actives.indexOf(i) > -1) {
        classes.push("active");
      }
      pinfo = outline[i];
      if (pinfo.depth === 0) {
        if (pinfo.hasChildren) {
          classes.push("dropdown");
          nb += '          <li class="' + classes.join(" ") + '">\n';
          nb +=
            '            <a href="#" class="dropdown-toggle" data-toggle="dropdown">';
          nb += pinfo.text;
          //nb += ' ' + i;
          nb += '<span class="caret"></span>';
          nb += "</a>\n";
          // submenu here
          nb += '            <ul class="dropdown-menu" role="menu">\n';
          for (j = i + 1; j < l; j++) {
            subclasses = [];
            if (actives.indexOf(j) > -1) {
              subclasses.push("active");
            }
            psinfo = outline[j];
            if (psinfo.depth === 0) {
              break;
            } else if (psinfo.depth === 1) {
              nb += "              <li";
              if (classes.length) {
                nb += ' class="' + subclasses.join(" ") + '"';
              }
              nb += ">";
              var lurl = psinfo.url;
              if (!lurl) {
                lurl = outline[getNext(j, pages)].url;
              }
              if (lurl) {
                nb += '<a href="' + lurl + '">';
                nb += psinfo.text;
                //nb += ' ' + i + ',' + j;
                nb += "</a>";
              } else {
                nb += psinfo.text;
              }
              nb += "              </li>\n";
            }
          }
          nb += "            </ul>\n";
          nb += "          </li>\n";
        } else if (pinfo.url) {
          nb += '          <li><a href="' + pinfo.url + '">';
          nb += pinfo.text;
          nb += "</a></li>\n";
        } else {
          nb += "          <li>" + pinfo.text + "</li>";
        }
      }
    }
    nb +=
      '        </ul>\n\
        <ul class="nav navbar-nav navbar-right">\n\
          <li><a id="weUser" href="#" class="navbar-link"></a></li>\n\
        </ul>\n\
      </div>\n\
    </nav>\n\
  </div>\n\
</header>\n';

    /*
    // third level navbar
    if (!opt['--rthird'] && outline[pi].depth === 2) {
      nb += '<nav class="navbar navbar-default" role="navigation">\n\
            <div class="container-fluid">\n\
              <div class="navbar-header">\n\
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#l2nav">\n\
                  <span class="sr-only">Toggle navigation</span>\n\
                  <span class="glyphicon glyphicon-th-list"></span>\n\
                </button>\n\
              </div>\n\
              <div class="collapse navbar-collapse" id="l2nav">\n\
                <ul class="nav navbar-nav">\n';
      pno = 1;
      l2start = pi;
      while (l2start > 0) {
        if (outline[l2start-1].depth !== 2) {
          break;
        }
        l2start--;
      }
      l2end = pi;
      while (l2end < outline.length) {
        if (outline[l2end].depth !== 2) {
          break;
        }
        l2end++;
      }
      var l2text;
      for (l2=l2start; l2<l2end; l2++) {
        nb += '<li' + ((l2 === pi) ? ' class="active"' : '') + '><a href="' + outline[l2].url;
        //l2text = pno + '';
        l2text = '<span class="glyphicon glyphicon-file"></span>';
        if (opt['--completethird']) {
          nb += '" title="' + outline[l2].text + '">' + outline[l2].text + '</a></li>\n';
        } else {
          nb += '" title="' + outline[l2].text + '">' + (((l2 >= pi-1) && (l2 <= pi+1)) ? outline[l2].text : l2text) + '</a></li>\n';
        }
      }

      nb += '   </ul>\n\
        </div>\n\
      </div>\n\
    </nav>\n';
    }
    */

    $("body").prepend(nb);
  },

  prevnext: function($, pl, nl) {
    var pager =
      '    <div class="row">\n\
      <div class="col-md-12">\n\
        <ul class="pager">\n';
    if (pl) {
      pager +=
        '          <li class="previous">\n\
            <a href="' +
        pl +
        '">&larr; Previous</a>\n\
          </li>\n';
    }
    if (nl) {
      pager +=
        '          <li class="next">\n\
            <a href="' +
        nl +
        '">Next &rarr;</a>\n\
          </li>\n';
    }
    pager += "        </ul>\n\
      </div>\n\
    </div>\n";
    $("#main>div").append(pager);
  },

  process: function($, opt) {
    $('script[src*="wikieducator.org/load.php"]').remove();
    $("#top").remove();

    // restyle iDevices
    $(".eXe-iDevice").each(function() {
      var classes,
        $inner,
        $idtitle,
        idtitle,
        idbody,
        idclasses,
        idquizzes = "",
        idtype = $(this).data("iDevice"),
        idtheme = $(this).data("theme") || "line",
        idid = $(this).attr("id"),
        ididStr = idid ? ` id="${idid}"` : "";

      idtheme = idtheme.toLowerCase();
      if (!idtype) {
        classes = $(this).attr("class").split(" ");
        idtype = classes.filter(function(e) {
          return e !== "eXe-iDevice" && e !== "iDevice";
        })[0];
      }
      // WordPress theme uses spaces in iDevice types
      idtype = $.trim(idtype.replace(/_+/g, " "));

      // check for any WE classes, and add them to the shortcode
      // and if there is a quiz, signal the plugin
      idclasses = $(this).attr("class");
      if (idclasses) {
        idclasses = idclasses.split(" ").filter(e => e.slice(0, 2) === "WE");
        if (idclasses.length) {
          if (idclasses.find(e => e.slice(0, 6) === "WEquiz")) {
            idquizzes = "[WEquizzes]";
          }
          idclasses = ` classes="${idclasses.join(" ")}"`;
        }
      } else {
        idclasses = "";
      }

      // process new style Template:IDevice separately
      if ($(this).hasClass("iDevice")) {
        idtitle = $(this).find(".iDevice-title").html();
        idbody = $(this).find(".iDevice-body").html();
      } else {
        // in old iDevices there are (at least) three places where the title might be
        //   <div class="iDevice"><div><h1>title</h1></div>
        //   <div class="iDevice"><div style="{big}">title</div>
        //   <div class="iDevice"><h1>title</h1>
        $inner = $(this).children("div>div>h1");
        if ($inner.length) {
          $idtitle = $inner.first();
        } else {
          $inner = $(this).find('div[style*="font-size"]');
          if ($inner.length) {
            $idtitle = $inner.first();
          } else {
            $idtitle = $(this).find("h1:first");
          }
        }
        // get rid of any wrapping span
        $inner = $idtitle.find('span[class="mw-headline"]');
        if ($inner.length) {
          idtitle = $inner.html();
        } else {
          idtitle = $idtitle.html();
        }

        idtitle = $.trim(idtitle);
        idbody = $(this).find("table:first tr td").html();
      }
      $(
        this
      ).replaceWith(`[oeru_idevice${ididStr} type='${idtype}' title=${quotedWPstring(idtitle)}${idclasses}]
${idbody}
[/oeru_idevice]${idquizzes}
`);
    });

    // restyle boxes into panels
    $(".WEbox").find(".panel-primary:first").removeClass("panel-primary");
    $(".WEbox-header").each(function() {
      var contents = $(this).contents();
      $(this).next().prepend("<h2 />");
      $(this).next().find("h2").append(contents);
      $(this).remove();
    });
    $(".WEbutton").each(function() {
      var contents;
      $(this).find("a").removeAttr("class");
      $(this).removeAttr("style").removeClass("WEbutton").addClass("button");
    });

    // restyle "columns" tables into a row of panels
    $("table.columns").each(function() {
      var cols = $(this).find("tr:last td").length,
        gw = "col-md-" + Math.floor(12 / cols),
        ix,
        cHead,
        cBody,
        cc = [];
      for (ix = 0; ix < cols; ix++) {
        cHead = $(this).find("tr th").eq(ix).html();
        cBody = $(this).find("tr:eq(1) td").eq(ix).html();
        cc.push(`
  <div class="${gw}">
    <div class="panel">
      <div class="panel-heading">${cHead}</div>
      <div class="panel-body">${cBody}</div>
    </div>
  </div>
`);
      }
      $(this).replaceWith($('<div class="row" />').append(cc.join("\n")));
    });

    // tables
    $("table.oeru1, table.prettytable")
      .addClass("table")
      .addClass("table-striped");
    // images
    if (opt["--wpurl"]) {
      // broken image links
      $('a[href^="/Special:Upload"]').replaceWith(
        '<img src="MissingImage.png" title="Missing image">'
      );
      // rewrite links to image pages for attribution/licensing
      $("a.image").each(function() {
        var dst = $(this).attr("href");
        if (dst.indexOf("/File:") === 0) {
          $(this).attr("href", "https://wikieducator.org" + dst);
        }
      });
      // FIXME images should be uploaded to Wordpress rather than hotlinked
      $("img").each(function() {
        var p,
          dst = $(this).attr("src");
        if ($(this).hasClass("pedagogicalicon")) {
          /*
          p = dst.lastIndexOf('/');
          if (p > -1) {
            $(this).attr('src',
              '/wp-content/themes/oeru_course/idevices' + dst.slice(p));
          }
          */
        } else if (
          dst.length >= 2 &&
          dst.charAt(0) === "/" &&
          dst.charAt(1) !== "/"
        ) {
          dst = "//wikieducator.org" + dst;
          $(this).attr("src", dst);
          // FIXME rewrite sourceset srcs rather than removing them
          $(this).removeAttr("srcset");
        }
      });
    }
    $(".thumbinner").addClass("thumbnail");
    $("section#main")
      .find("img")
      .not(".pedagogicalicon")
      .addClass("img-responsive");
    $(".WEflag img").attr(
      "style",
      "vertical-align: baseline; display: inline;"
    );
    // Special: pages link directly to the wiki
    $('a[href^="/Special"]').each(function() {
      $(this).attr("href", "https://wikieducator.org" + $(this).attr("href"));
    });
    // override Bootstrap's img block in Template:Pdf
    $('.pdfdown img[alt="PDF down.png"]').css("display", "inline-block");
    // use WEnotes plugin
    $(".WEnotes").each(function() {
      var tag = $(this).data("tag"),
        count = $(this).data("count");
      $(this).replaceWith(`[WEnotes tag="${tag}" count="${count}"]`);
    });
    $(".WEnotesPost").each(function() {
      var tag = $.trim($(this).data("tag")),
        button = $.trim($(this).data("button")),
        buttonAttr = button ? ` button="${button}"` : "",
        leftmargin = $.trim($(this).data("leftmargin")),
        leftmarginAttr = leftmargin ? ` leftmargin="${leftmargin}"` : "";
      $(this).replaceWith(
        `[WEnotesPost tag="${tag}"${buttonAttr}${leftmarginAttr}]`
      );
    });
  }
};

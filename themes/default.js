/* jshint node: true, multistr: true, trailing: true */
module.exports = {
  version: "0.0.1",

  // emit headers
  header: function($, opt) {
    $("head").append(
      '\
<link rel="stylesheet" href="' +
        opt["--urlprefix"] +
        '/css/bootstrap.min.css">\n\
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">\n\
    <!--[if lt IE 9]>\n\
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>\n\
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>\n\
    <![endif]-->\n\
<link rel="stylesheet" href="' +
        opt["--urlprefix"] +
        '/css/we.css">\n'
    );
    $("body").append(
      '<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>\n\
  <script src="' +
        opt["--urlprefix"] +
        '/js/we.js"></script>\n'
    );
  },

  footer: function($, opt, pages, pi) {
    $("#footer")
      .addClass("container")
      .prepend(
        '<div class="ilogo"><a href="' +
          opt["--link"] +
          '"><img src="' +
          opt["--urlprefix"] +
          "/img/" +
          opt["--logo"] +
          '"></a></div>'
      );
    $("#footer-places").append(
      '<li><a href="http://wikieducator.org' +
        pages[pi] +
        '?action=history">Authors</a></li>'
    );
    // move the license icon
    $("#footer-copyrightico").detach().appendTo("#footer-places");
    $("#footer-icons").remove();
    $("#footer-copyrightico a").attr("rel", "license");
  },

  wrap: function($) {
    $("body").wrapInner('<div id="bodyInner"></div>');
    $("#content").wrap('<div id="WEcontentcontainer" class="container"/>');
  },

  nav: function($, opt, pages, pi, outline, getPrev, getNext) {
    var i,
      l,
      j,
      l2,
      l2start,
      l2end,
      pno,
      actives,
      pinfo,
      psinfo,
      subclasses,
      classes = [];
    var nb =
      '<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">\n\
    <div class="navbar-inner">\n\
    <div class="container-fluid">\n\
      <div id="course-name" class="navbar-header">\n\
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#wembar">\n\
          <span class="sr-only">Toggle navigation</span>\n\
          <span class="icon-bar"></span>\n\
          <span class="icon-bar"></span>\n\
          <span class="icon-bar"></span>\n\
        </button>\n\
        <a class="navbar-brand" href="' +
      opt["--urlprefix"] +
      '">' +
      opt["--brand"] +
      '</a>\n\
      </div>\n\
      <div id="wembar" class="collapse navbar-collapse">\n\
        <ul class="nav navbar-nav">\n';
    if (opt["--scan"]) {
      nb +=
        '<li>\n\
            <a id="scanpagetoggle" data-toggle="modal" data-target="#scanpage"><span style="font-size: 18px;" class="glyphicon glyphicon-tree-conifer"></span></a>\n\
          </li>\n';
    }
    l = outline.length;
    actives = outline[pi].path;
    for (i = 1; i < l; i++) {
      classes = [];
      if (actives.indexOf(i) > -1) {
        classes.push("active");
      }
      pinfo = outline[i];
      if (pinfo.depth === 0) {
        if (pinfo.hasChildren) {
          classes.push("dropdown");
          nb += '<li class="' + classes.join(" ") + '">';
          nb += '<a href="#" class="dropdown-toggle" data-toggle="dropdown">';
          nb += pinfo.text;
          //nb += ' ' + i;
          nb += ' <b class="caret"></b>';
          nb += "</a>";
          // submenu here
          nb += '<ul class="dropdown-menu">\n';
          for (j = i + 1; j < l; j++) {
            subclasses = [];
            if (actives.indexOf(j) > -1) {
              subclasses.push("active");
            }
            psinfo = outline[j];
            if (psinfo.depth === 0) {
              break;
            } else if (psinfo.depth === 1) {
              nb += "<li";
              if (classes.length) {
                nb += ' class="' + subclasses.join(" ") + '"';
              }
              nb += ">";
              var lurl = psinfo.url;
              if (!lurl) {
                lurl = outline[getNext(j, pages)].url;
              }
              if (lurl) {
                nb += '<a href="' + lurl + '">';
                nb += psinfo.text;
                //nb += ' ' + i + ',' + j;
                nb += "</a>";
              } else {
                nb += psinfo.text;
              }
              nb += "</li>\n";
            }
          }
          nb += "</ul>\n";
          nb += "</li>\n";
        } else if (pinfo.url) {
          nb += '<li><a href="' + pinfo.url + '">';
          nb += pinfo.text;
          nb += "</a></li>\n";
        } else {
          nb += "<li>" + pinfo.text + "</li>";
        }
      }
    }
    nb +=
      '</ul>\n\
    <p class="navbar-text navbar-right"><a id="weUser" href="#" class="navbar-link"></a></p>\n\
    </div>\n\
    </div>\n\
    </div>\n\
  </nav>\n';

    // third level navbar
    if (!opt["--rthird"] && outline[pi].depth === 2) {
      nb +=
        '<nav class="navbar navbar-default" role="navigation">\n\
            <div class="container-fluid">\n\
              <div class="navbar-header">\n\
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#l2nav">\n\
                  <span class="sr-only">Toggle navigation</span>\n\
                  <span class="glyphicon glyphicon-th-list"></span>\n\
                </button>\n\
              </div>\n\
              <div class="collapse navbar-collapse" id="l2nav">\n\
                <ul class="nav navbar-nav">\n';
      pno = 1;
      l2start = pi;
      while (l2start > 0) {
        if (outline[l2start - 1].depth !== 2) {
          break;
        }
        l2start--;
      }
      l2end = pi;
      while (l2end < outline.length) {
        if (outline[l2end].depth !== 2) {
          break;
        }
        l2end++;
      }
      var l2text;
      for (l2 = l2start; l2 < l2end; l2++) {
        nb +=
          "<li" +
          (l2 === pi ? ' class="active"' : "") +
          '><a href="' +
          outline[l2].url;
        //l2text = pno + '';
        l2text = '<span class="glyphicon glyphicon-file"></span>';
        if (opt["--completethird"]) {
          nb +=
            '" title="' +
            outline[l2].text +
            '">' +
            outline[l2].text +
            "</a></li>\n";
        } else {
          nb +=
            '" title="' +
            outline[l2].text +
            '">' +
            (l2 >= pi - 1 && l2 <= pi + 1 ? outline[l2].text : l2text) +
            "</a></li>\n";
        }
      }

      nb +=
        "   </ul>\n\
              </div>\n\
            </div>\n\
          </nav>\n";
    }

    $("body").prepend(nb);
  },

  prevnext: function($, pl, nl) {
    $("#bodyContent").after('<ul id="weBottomNav" class="pager"></ul>');
    if (pl) {
      $("#weBottomNav").append(
        '<li class="previous"><a href="' + pl + '">&larr; Prev</a></li>'
      );
    }
    if (nl) {
      $("#weBottomNav").append(
        '<li class="next"><a href="' + nl + '">Next &rarr;</a></li>'
      );
    }
  },

  process: function($) {
    $(".WEbutton").removeAttr("style").removeClass(".WEbutton");
    $("section#main").find("img").addClass("img-responsive");
  }
};

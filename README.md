# course

This is a simple demonstration of using
[jsdom](https://github.com/tmpvar/jsdom)
and [jQuery](http://jquery.com/) in Node.js
to _snapshot_ a collection (outline) of pages stored in WikiEducator.
It modifies the pages as they are fetched, and writes a static tree
of them to the filesystem or to a WordPress instance.

## Installation

These sample scripts require [Node.js](https://nodejs.org/en/) 6.x.x or greater.

> If you don't already have Node.js installed, using
> [nvm](https://github.com/creationix/nvm) is an easy
> way to proceed.
>
> You can install nvm using cURL:
>
>   `curl https://raw.githubusercontent.com/creationix/nvm/v0.33.2/install.sh | bash`
>
> *or* Wget:
>
>   `wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.33.2/install.sh | bash`
>
> Then use nvm to install a recent version of [node.js](https://nodejs.org/en/):
>
>   `nvm install v6.11.0`
>
> For future invocations now that Node.js is installed, you merely
> need to tell your shell which version to use with
> `nvm use v6.11.0`

1. git clone https://bitbucket.org/wikieducator/course.git
2. cd course
3. npm install

## Use

This script currently has two very different functions:

* make a snapshot of static HTML files in the local filesystem
* build a snapshot of the course in a WordPress installation
  using XMLRPC

Many of the *options* are actually required to produce meaningful output.

To make filesystem snapshots, you **must** set FILEPATH
at the top of course.js before use.  FILEPATH must be a
top level directory that exists within your web server
root (so that the course files can be served to users).

### Filesystem snapshot

    node course.js \
        {--prevnext} \
        --brand HEADERBRAND \
        --urlprefix first-part-of-url
        {--theme THEMENAME} \
        {--logo logopath} \
        {--link http://link-for-logo} \
        http://WikiEducator.org/OutlinePage \
        /localfilesystem/path

### WordPress snapshot

    node courseWP.js \
        {--prevnext} \
        --brand HEADERBRAND \
        --wpurl http://wordpressURL \
        --wpuser USERNAME --wppass PASSWORD \
        {--wpmenu} \
        {--wpdelete} \
        {--theme THEMENAME} \
        {--logo logopath} \
        {--link http://link-for-logo} \
        {--siteid SITEID} \
        {--wiki base_url} \
        {--debugout} \
        http://wikieducator.org/path-to-outline-page \
        "/Course name"

## N.B.

These scripts are *simple examples* of
[ways you might make snapshots](http://wikieducator.org/User:JimTittsler/Theming#Course_Snapshot)
of course-sized open educational resources stored in WikiEducator.
I expect that different institutions (and even different courses
within an institution) will have different needs, organizations, and
styling.

Update 2019-07-23: added iFrame embedding capability using the new [WEiFrame template](https://wikieducator.org/Template:WEiFrame) on WikiEducator.

The main concern with this is that if the site you're wanting to include in the iframe has "X-Frame-Options" (in the sites website configuration, e.g. Apache or NGINX) is set to "sameorigin". That will block the iframe display. 

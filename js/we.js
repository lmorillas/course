/* jshint node: true, multistr: true, trailing: true */
$(function() {
  var loc;

  function loginComplete() {
    var iframe = $('#WEregistrationIFrameDiv iframe').get(0);
    if (iframe && iframe.contentWindow) {
      var href = iframe.contentWindow.location.href;
      if (href.indexOf('/Course') > -1) {
        window.location.reload(true);
      }
    }
  }

  function showLogin() {
    function iFrameSrc(s) {
      var $iframeDiv = $('#WEregistrationIFrameDiv');
      $iframeDiv.find('iframe')
        .attr('src', 'http://wikieducator.org/index.php?returnto=Course&title=Special:User' + s);
      $iframeDiv.find('h4').text('WikiEducator ' + s);
    }

    if ($('#WEregistrationIFrameDiv').length === 0) {
      $('body').append('\n\
        <div id="WEregistrationIFrameDiv" class="modal fade">\n\
          <div class="modal-dialog">\n\
            <div class="modal-content">\n\
              <div class="modal-header">\n\
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>\n\
                <h4>WikiEducator login</h4>\n\
              </div>\n\
              <div class="modal-body">\n\
                <iframe src="http://wikieducator.org/index.php?title=Special:UserLogin&returnto=Course" width=500 height=510></iframe>\n\
              </div>\n\
            </div>\n\
          </div>\n\
        </div>\n');
    }
    iFrameSrc(wgUserName ? 'Logout' : 'Login');
    $('#WEregistrationIFrameDiv').modal('show');

    // periodically check the IFrame source
    setInterval(loginComplete, 500);
  }

  window.loginWE = function(note) {
    showLogin();
  }

  $('#weUser').click(showLogin);
  $('.dropdown-toggle').dropdown();

  function setuplogin() {
    $('a[href*="Special:UserLogin"]').click(function() {
      window.loginWE();
      return false;
    });
    /*
      // registration page
      if ($('div.WEcourseRegistration').length) {
        $('body').append('<div id="WEregistrationIFrame">\n\
              <iframe src="http://wikieducator.org/index.php?title=Special:UserLogin&returnto=Practice:OCL4Ed/_Registration" width=500 height=520></iframe>\n\
            </div>\n');
      }
    if ($('#WEcourseFormGoLogin').length) {
      // dynamically load jQueryUI
      $('head').append('<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/smoothness/jquery-ui.css" type="text/css" />');
      $.getScript('https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js' );
    }

    $('#WEcourseFormGoLogin').click(function() {
      $('#WEregistrationIFrame').dialog({
        height: 650,
        width: 550,
        modal: true,
        title: 'Login/Create account on WikiEducator',
        position: {within: '#content'}
        //position: {my: 'top', at: 'top', of: '#content'}
      });
      return false;
    });
    */
  }
  setTimeout(setuplogin, 1000);

  // if the table specified cellpadding, duplicate that for its ths and tds
  $('table').each(function() {
    var cellpadding = $(this).attr('cellpadding');
    if (cellpadding) {
      $(this).find('th, td').css('padding', cellpadding + 'px');
    }
  });

  /*
  var uname = $.cookie('wikiedu_mw_UserName');
  console.log('read cookie', uname, $.cookie('_ga'), $.cookie('wkedu_sess'), $.cookie('wikiedu_mw_OpenID'));
  console.log($.cookie());
  if (uname) {
    $('#weUser').text(uname);
  } else {
    $('#weUser').html('<a href="/course/OCL4Ed/Registration/">Login</a>');
  }
  */

  function setUser() {
    if (window.wgUserName2) {
      window.wgUserName = window.wgUserName2;
      $('#weUser').text(window.wgUserRealName || window.wgUserName);
    } else {
      $('#weUser').html('<span class="glyphicon glyphicon-user"></span>');
    }
  }

  if (window.wgUserLoading === 1) {
    // still waiting
    window.wgUserCallback = setUser;
  } else if (window.wgUserLoading > 1) {
    setUser();
  }

  // activate the current location in the SCAN page
  //loc = window.location.href.replace('http://wikieducator.org', '');
  old = window.location.href;
  loc = window.location.href.replace('wikieducator.org', '');
  console.log('************* old:'+old+'->loc:'+loc);
  $('#scanpage a[href="' + loc + '"]').addClass('active')
      .addClass('btn-success')
      .removeClass('btn-primary')
      .removeClass('btn-info');

  // count SCAN page activity
  $('#scanpagetoggle').click(function() {
    hewlettOERTracker('we.send', 'pageview', {
      'page': '/course/OCL4Ed/SCAN',
      'title': 'SCAN popover'
    });
    return true;
  });

  // on the Console page, may need to open some disclosure triangles
  if (loc === '/course/OCL4Ed/Console/#Course_schedule') {
    $('#collapse0').addClass('in');
    $('#collapse0').parent().find('a').removeClass('collapsed');
  } else if (loc === '/course/OCL4Ed/Console/#Announcements') {
    $('#collapse1').addClass('in');
    $('#collapse1').parent().find('a').removeClass('collapsed');
  }
});
